import { InMemoryCache } from 'apollo-cache-inmemory'
import { WebSocketLink } from 'apollo-link-ws';
const getHeaders = () => {
    const headers = {}
    const token = localStorage.getItem('token')
    if (token) {
        headers.authorization = `Bearer ${token}`
    }
    return headers
}

export default function (context) {
    return {
        httpLinkOptions: {
            uri: 'http://localhost:8080/v1/graphql',
            credentials: 'same-origin',
            headers: getHeaders(),
        },
        cache: new InMemoryCache(),
        wsEndpoint: 'ws://localhost:8080/v1/graphql',
    }
  // Create a WebSocket link:
//    new WebSocketLink({
//     uri: 'ws://localhost:8080/v1/graphql',
//     options: {
//       reconnect: true,
//       lazy: true,
//     //   timeout: 50000,
//       connectionParams: () => {
//         return { headers: getHeaders(),
//             cache: new InMemoryCache(),
//             wsEndpoint: 'ws://localhost:8080/v1/graphql',
//         };
//       },
//     }
//   });
          // Create a WebSocket link:
//     return  new WebSocketLink({
//     uri: 'http://localhost:8080/v1/graphql',
//     options: {
//       reconnect: true,
//       timeout: 30000,
//       connectionParams: () => {
//         return { 
//             headers: getHeaders() ,
//             cache: new InMemoryCache(),
//             wsEndpoint: 'ws://localhost:8080/v1/graphql',
//         };
//       },
//     }
//   })
  
}
    