import en from './en.json'
import et from './et.json'
export default {
  locale: 'en',
  fallbackLocale: 'en',
  messages: { en, et }
}
