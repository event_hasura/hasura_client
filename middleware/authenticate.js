export default function({ store, redirect, route, app }) {
    if (!store.getters['user/logged_in']) {
      return redirect(
        '/login'
      )
    }
  }
  