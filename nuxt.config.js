import colors from 'vuetify/es5/util/colors';
import i18n from './locale/i18n';

export default {
  ssr: false,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - hasura_client',
    title: 'hasura_client',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', ref: "https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500&display=swap" }
    ]
  },
  loadingIndicator: {
    name:'~/loading.html'
  },
  loading: { color: '#344182' },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/rules'},
    { src: '~/plugins/apollo'},
    { src: '~/plugins/vex-persist.js' },
    { src: '~plugins/leaflet.js', ssr: false },
    { src: '~plugins/expandable-image.js' }
    
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: ["~/components", {path: "~/components/user/"}],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/vuetify',
    [
      'nuxt-i18n',
      {
        strategy: 'no_prefix',
        defaultLocale: 'en',
         locales: [
          {
             code: 'en',
             name: 'ENG'
          },
          {
             code: 'et',
             name: 'አማርኛ'
          }
        ],
        vueI18n: i18n
      }
    ]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'nuxt-leaflet',
    // '@nuxtjs/pwa'
    '@nuxtjs/toast',
    // '@nuxtjs/nuxt-i18n',

    
  ],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    defaultAssets: {
      font: {
        family: 'Dancing Script'
      }
    },
    treeShaking: true,
    // theme: {
    // dark: false,
    // themes: {
    //   dark: {
    //     primary: colors.teal.darken2,
    //     accent: colors.grey.darken3,
    //     secondary: colors.amber.darken3,
    //     info: colors.teal.lighten1,
    //     warning: colors.amber.base,
    //     error: colors.deepOrange.accent4,
    //     success: colors.green.accent3
    //   }
    // }
    // }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  
    build: {
      extend(config, ctx) {
        config.module.rules.push({
          test: /\.(graphql|gql)$/,
          exclude: /node_modules/,
          loader: 'graphql-tag/loader'
        })
      }
    },
    // vendor: ['vue2-google-maps'],
    toast: {
      position: 'top-right',
      duration: 5000,
      keepOnHover: true
    },
  }
