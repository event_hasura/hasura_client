import Vue from 'vue'
import VueApollo from 'vue-apollo'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { split, ApolloLink, from } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'
import { onError } from 'apollo-link-error'

export default function({ store, app }) {
  const connectionParams = () => {
    return {
      headers: {
        authorization: store.getters['user/logged_in']
          ? `Bearer ${store.getters['user/token']}` 
          : undefined
      }
    }
  }

  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (
      graphQLErrors &&
      graphQLErrors[0].extensions &&
      (graphQLErrors[0].extensions.code === 'invalid-jwt' ||
        graphQLErrors[0].extensions.code === 'start-failed')
    ) {
      store.commit('user/logout')
      sessionStorage.removeItem('session')
      localStorage.removeItem('session')
      app.$toast.error('session expired! please login')
      app.router.replace('/')
      return
    }

    if (
      networkError &&
      networkError.extensions &&
      (networkError.extensions.code === 'invalid-jwt' ||
        networkError.extensions.code === 'start-failed')
    ) {
      store.commit('user/logout')
      sessionStorage.removeItem('session')
      localStorage.removeItem('session')
      app.$toast.error('session expired! please login')
      app.router.replace('/')
      return
    }

    if (networkError) {
      app.$toast.error('network error! please check your connection')
    }
  })

  const etipHTTPLink = new HttpLink({
    uri: 'http://localhost:8080/v1/graphql',
  })

  const etipWSLink = new WebSocketLink({
    uri: 'ws://localhost:8080/v1/graphql',
    options: {
      reconnect: true,
      lazy: true,
      connectionParams
    }
  })

  let authLink = (operation, forward) => {
    const { headers } = operation.getContext()
    if (store.getters['user/logged_in']) {
      operation.setContext({
        headers: {
          authorization: `Bearer ${store.getters['user/token']}`,
          ...headers
        }
      })
    }
    return forward(operation)
  }

  const getDefinition = ({ query }) => {
    const definition = getMainDefinition(query)
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    )
  }

  const etipLink = split(getDefinition, etipWSLink, etipHTTPLink)

  authLink = new ApolloLink(authLink)

  const etipApolloClient = new ApolloClient({
    link: from([errorLink, authLink, etipLink]),
    cache: new InMemoryCache({
      addTypename: false
    }),
    connectToDevTools: true
  })

  const apolloProvider = new VueApollo({
    defaultClient: etipApolloClient,
    clients: {
      etip: etipApolloClient
    }
  })

  Vue.use(VueApollo)

  Vue.mixin({
    apolloProvider
  })
}
