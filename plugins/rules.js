import Vue from 'vue'

export default function() {
  Vue.mixin({
    props: {
      rules: {
        type: Object,
        default: () => ({
          required: [(v) => !!v || 'This field is required'],
          pdf: [
            (v) => (v && v.type.includes('pdf')) || 'Only pdf files are allowed'
          ],
          onechar: (v) => (v && v.length > 1) || '',
          // tin_number: [(v) => !v || /^\d{10}$/.test(v) || 'Invalid tin number'],
          email: [
            (v) =>
              !v ||
              /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
                v
              ) ||
              'Invalid Email Address'
          ],
          password_length: [(v) => !v || v.length >= 8 || 'Password too short'],
          ethiopian_phone_number: [
            (v) =>
              !v || /^\+251\d{9}$/.test(v) || 'Invalid Ethiopian Phone Number '
          ],

          et_phone_number_nullable: [
            (v) =>
              !v ||
              /^\+251\d{0}$/.test(v) ||
              /^\+251\d{9}$/.test(v) ||
              'Invalid Ethiopian Phone Number '
          ],
          et_phone_number_null_check: (v) =>
            !v || /^\+251\d{0}$/.test(v) || false,
          titleRule: [
              (v) => !!v || "Title is required",
              (v) => (v && v.length >= 5) || "Title must be less than 5 characters",
            ],
            priceRule: [
              (v) => !!v || "Price field is required",
              (v) => (v && parseInt(v) >= 0) || "Price must be greater than 0.",
            ],
            ticketRule: [
              (v) => !!v || "Total ticket field is required",
              (v) =>
                (v && parseInt(v) >= 1) || "Total ticket must be greater than 1.",
            ],
        })
      }
    }
  })
}
