import VuexPersistedstate from 'vuex-persistedstate';

export default ({ store }) => {
    VuexPersistedstate({
        storage: window.localStorage
    })(store);
}