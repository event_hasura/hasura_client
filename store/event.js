
export const state = () => ({
    events: [],
    // foundEvent: [],
    forPage:[],
    // addedE:[]
});

export const getters = {
    getEvents: state => { return state.events },
    getFoundEvent:state => {return state.foundEvent},
    getEventForPage: state => {return state.forPage},
    // getAddedE: state => {return state.addedE}
};

export const mutations = {
    clear_data(state){
        state.events = undefined
        state.forPage = undefined
    },
    setEvents(state, events) {
        state.events = events;
    },
    removeEvents(state, i) {
        state.events.splice(i, 1);
    },
    addnewEvent(state, event) {
        state.events.push(event);
    },
    updateEventImage(state, images, i) { //add Image
        state.events[i].hasManyImage.push(images);
    },
    removeAllEvents(state) {
        state.events = []
    },
    deleteEventImage(state, index, images) {
        state.events[index].hasManyImage = images
    },
    setEventForPage(state, events){
        state.forPage = events;
    },
    setFoundEvent(state, event){
        state.foundEvent = event;
    },
    // setAddedE(state, e){
    //     state.addedE.push(e);
    // },
    // setEmptyAdded(state){
    //     state.addedE = []
    // }
};

export const actions = {
    setEvents_a({ commit }, events) {
        commit("setEvents", events);
    },
    removeEvents_a({ commit, state }, id) {
        for (let i = 0; i < state.events.length; i++) {
            if (state.events[i].id === id) {
                commit("removeEvents", i);
                break;
            }
        }

    },
    // setAddedE_a({commit}, e){ //add event conditional for test
    //     commit("setAddedE",e);
    // },
    addNewEvent_a({ commit }, event) {
        commit("addnewEvent", event)
    },
    // setEmptyAdded_a({commit}){
    //     commit("setEmptyAdded")
    // },
    removeAllEvents_a({ commit }) {
        commit('removeAllEvents');
    },
    setFoundEvent_a({commit}, event){
        commit("setFoundEvent", event);
    },
    setEventForPage_a({commit}, events){
        commit("setEventForPage", events)
    },
    updateEventInfo_a({ commit, state }, event) {
        for (let i = 0; i < state.events.length; i++) {
            if (state.events[i].id == event.id) {
                state.events.splice(i, 1);
                commit("addnewEvent", event);
                break;
            }
        }
    },
    deleteEventImage_a({ commit, state }, allInfo) {
        // console.log(id)
        console.log(allInfo)
        console.log(allInfo.allImage)
        for (let i = 0; i < allInfo.allImage.length; i++) {
            if (allInfo.allImage[i].id === allInfo.imageId) {
                allInfo.allImage.splice(i, 1);//remove image
                // let updatedImage = allInfo.allImage
                console.log(allInfo.allImage)
                allInfo.event.hasManyImage = allInfo.allImage
                console.log(allInfo.event)
                for (let k = 0; k < state.events.length; k++) {
                    if (state.events[i].id == allInfo.event.id) {
                        // console.length(state.events[i])
                        state.events.splice(i, 1);
                        commit("addnewEvent", allInfo.event);
                        break;
                    }
                }
            }
        }
    }
}