import me from '~/apollo/me.gql'

export const state = () => ({
    userId: null,
    username: '',
    path: '',
    email: '',
    token: '',
    tickets: [],
    user:undefined,
    // profile: undefined
});

export const getters = {
    userId: state => { return state.userId},
    username: state => { return state.username },
    email: state => { return state.user.email},
    path: state => { return state.path },
    // token: state => {return state.token},
    getTickets: state => {return state.tickets},
    getLoggedinData : state => {return state.user},
    token(state) { return state.user ? state.user.access_token : undefined },
    logged_in(state) { return state.user !== undefined && state.user !== null },
};

export const mutations = {
    login(state,user){
        state.user = user;
    },
    logout(state) {
        state.username = undefined
        state.user = undefined
        state.email = undefined
        // state.profile = undefined
        localStorage.removeItem('session')
        // localStorage.removeItem('profile')
        state.ticket = undefined
        state.userId = undefined
    },
    set_user_data(state, profile){
        state.profile = profile;
    },
    setUsername(state, username) {
        state.username = username
    },
    setUserId(state, id) {
        state.userId = id
    },
    // setEmail(state, email) {
    //     state.email = email
    // },
    setPath(state, path) {
        state.path = path
    },
    // setToken(state, token){
    //     state.token = token;
    // },
    setTickets(state, tickets){
        state.tickets = tickets;
    },
    addTickets(state, ticket){
        state.tickets.push(ticket)
    }
};

export const actions = {
    // setToken_a({commit}, token){
    //     commit("setToken",token)
    // },
    setUsername_a({ commit }, username) {
        commit('setUsername', username)
    },
    setUserId_a({ commit }, id) {
        commit("setUserId", id)
    },
    setEmail_a({ commit }, email) {
        commit('setEmail', email)
    },
    setPath_a({ commit }, path) {
        commit("setPath", path)
    },
    // setGoogleId_a({ commit }, id) {
    //     commit("setGoogleId", id)
    // },
    setTickets_a({commit}, tickets){
        commit("setTickets", tickets)
    },
    addTickets_a({commit}, ticket){
        commit("addTickets", ticket)
    },
    // async fetch_user_data(ctx) {
    //     const { commit, getters } = ctx
    //     setTimeout(async () => {
    //       const client = this.$router.app.$apolloProvider.defaultClient
    //       const { data } = await client.query({
    //         query: me,
    //         manual: true,
    //         context: {
    //           headers: {
    //             authorization: `Bearer ${getters.token}`
    //           }
    //         },
    //         fetchPolicy: 'network-only'
    //       })
    //       if (data.me) {
    //           console.log("me", data.me)
    //         localStorage.setItem('profile', JSON.stringify(data.me))
    //         commit('set_user_data', data.me)
    //       }
    //     }, 1000)
    //   }
}